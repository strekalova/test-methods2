package ru.nsu.fit.endpoint.exception;

/**
 * Created by Владимир on 01.10.2016.
 */
public class InvalidFirstNameUserException extends IllegalArgumentException {

    public final static String FN_LEN_LESS = "Last name's length must be greater than ";
    public final static String FN_LEN_GRET = "Last name's length must be lesser than ";
    public final static String FN_RULES = "Last name must be started from UPPER case symbol, other symbols must be lowercase letters";

    public InvalidFirstNameUserException(String p0) { super(p0);
    }
}
