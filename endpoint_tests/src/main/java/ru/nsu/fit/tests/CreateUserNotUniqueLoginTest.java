package ru.nsu.fit.tests;

import org.glassfish.jersey.client.ClientConfig;
import org.glassfish.jersey.client.authentication.HttpAuthenticationFeature;
import org.glassfish.jersey.jackson.JacksonFeature;
import org.testng.Assert;
import org.testng.annotations.Test;
import ru.nsu.fit.shared.AllureUtils;
import ru.nsu.fit.shared.RandomDataGenerator;
import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Title;
import ru.yandex.qatools.allure.model.SeverityLevel;

import javax.ws.rs.client.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Title("Create user")
public class CreateUserNotUniqueLoginTest {

    @Test
    @Description("Create user with not unique login")
    @Severity(SeverityLevel.CRITICAL)
    @Features("User create feature")
    public void createUser() {
        ClientConfig clientConfig = new ClientConfig();

        String userLogin, customerLogin, customerPass, uid;

        synchronized (CreateUserTest.mutex) {
            CreateUserTest.createUser();
            userLogin = CreateUserTest.login;
            customerLogin = CreateUserTest.customerLogin;
            customerPass = CreateUserTest.customerPass;
            uid = CreateUserTest.uid;
        }

        HttpAuthenticationFeature feature = HttpAuthenticationFeature.basic(customerLogin, customerPass);
        clientConfig.register(feature);

        clientConfig.register(JacksonFeature.class);

        Client client = ClientBuilder.newClient( clientConfig );

        WebTarget webTarget = client.target("http://localhost:8080/endpoint/rest").path("create_user/" + uid);
        Invocation.Builder invocationBuilder =	webTarget.request(MediaType.APPLICATION_JSON);
        Response response = invocationBuilder.post(Entity.entity("{" +
                "\"firstName\":\""+"Defa"+"\"," +
                "\"lastName\":\""+"Defa"+"\"," +
                "\"login\":\""+userLogin+"\"," +
                "\"pass\":\""+"DeFa_AAA123"+"\"," +
                "\"userRole\":\""+"USER"+"\"" +
                "}", MediaType.APPLICATION_JSON));
        String out = response.readEntity(String.class);
        AllureUtils.saveTextLog("Response: " + out);
        Assert.assertTrue(response.getStatus() != 200);
        Assert.assertTrue(out.contains("Duplicate entry"));
    }
}
