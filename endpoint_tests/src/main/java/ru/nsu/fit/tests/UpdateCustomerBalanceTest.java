package ru.nsu.fit.tests;

import org.glassfish.jersey.client.ClientConfig;
import org.glassfish.jersey.client.authentication.HttpAuthenticationFeature;
import org.glassfish.jersey.jackson.JacksonFeature;
import org.testng.Assert;
import org.testng.annotations.Test;
import ru.nsu.fit.shared.AllureUtils;
import ru.yandex.qatools.allure.annotations.*;
import ru.yandex.qatools.allure.model.SeverityLevel;

import javax.ws.rs.client.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Title("Update customer balance")
public class UpdateCustomerBalanceTest {

    @Test
    @Description("Update customer balance")
    @Severity(SeverityLevel.MINOR)
    @Features("Update balance")
    public void updateCustomerBalance() {

        String customerLogin, customerPass, uid;
        int money;
        synchronized (GetCustomerIdByLogin.mutex) {
            GetCustomerIdByLogin.getCustomerId();
            customerLogin = GetCustomerIdByLogin.customerLogin;
            customerPass = GetCustomerIdByLogin.customerPass;
            money = GetCustomerIdByLogin.money;
            uid = GetCustomerIdByLogin.uid;
        }

        ClientConfig clientConfig = new ClientConfig();

        HttpAuthenticationFeature feature = HttpAuthenticationFeature.basic(customerLogin, customerPass);
        clientConfig.register(feature);

        clientConfig.register(JacksonFeature.class);

        Client client = ClientBuilder.newClient( clientConfig );

        WebTarget webTarget = client.target("http://localhost:8080/endpoint/rest").path("add_money/" + uid + "/100");

        Invocation.Builder invocationBuilder = webTarget.request(MediaType.APPLICATION_JSON);
        Response response = invocationBuilder.get();

        String responseString = response.readEntity(String.class);
        AllureUtils.saveTextLog("Response: " + responseString);
        Assert.assertEquals(response.getStatus(), 200);
        Assert.assertTrue(Integer.parseInt(responseString.substring(15, responseString.length()- 3)) == money + 100);
    }
}
