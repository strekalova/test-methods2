package ru.nsu.fit.tests;

import org.glassfish.jersey.client.ClientConfig;
import org.glassfish.jersey.client.authentication.HttpAuthenticationFeature;
import org.glassfish.jersey.jackson.JacksonFeature;
import org.testng.Assert;
import org.testng.annotations.Test;
import ru.nsu.fit.shared.AllureUtils;
import ru.nsu.fit.shared.RandomDataGenerator;
import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Title;
import ru.yandex.qatools.allure.model.SeverityLevel;

import javax.ws.rs.client.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Title("Create user")
public class CreateUserTest {

    public static final Object mutex = new Object();
    public static String fName, lName, login, pass, userRole;
    public static String customerLogin, customerPass, uid;

    @Test
    @Description("Create user via REST API")
    @Severity(SeverityLevel.BLOCKER)
    @Features("User create feature")
    public static void createUser() {
        ClientConfig clientConfig = new ClientConfig();

        synchronized (GetCustomerIdByLogin.mutex) {
            GetCustomerIdByLogin.getCustomerId();
            customerLogin = GetCustomerIdByLogin.customerLogin;
            customerPass = GetCustomerIdByLogin.customerPass;
            uid = GetCustomerIdByLogin.uid;
        }

        HttpAuthenticationFeature feature = HttpAuthenticationFeature.basic(customerLogin, customerPass);
        clientConfig.register(feature);

        clientConfig.register(JacksonFeature.class);

        Client client = ClientBuilder.newClient( clientConfig );

        fName = RandomDataGenerator.generateName();
        lName = RandomDataGenerator.generateName();
        login = RandomDataGenerator.generateLogin();
        pass = RandomDataGenerator.generatePassword();
        userRole = "USER";

        WebTarget webTarget = client.target("http://localhost:8080/endpoint/rest").path("create_user/" + uid);
        Invocation.Builder invocationBuilder =	webTarget.request(MediaType.APPLICATION_JSON);
        Response response = invocationBuilder.post(Entity.entity("{" +
                "\"firstName\":\""+fName+"\"," +
                "\"lastName\":\""+lName+"\"," +
                "\"login\":\""+login+"\"," +
                "\"pass\":\""+pass+"\"," +
                "\"userRole\":\""+userRole+"\"" +
                "}", MediaType.APPLICATION_JSON));
        String out = response.readEntity(String.class);
        AllureUtils.saveTextLog("Response: " + out);
        Assert.assertTrue(response.getStatus() == 200);
    }
}
