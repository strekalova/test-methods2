package ru.nsu.fit.tests;

import org.glassfish.jersey.client.ClientConfig;
import org.glassfish.jersey.client.authentication.HttpAuthenticationFeature;
import org.glassfish.jersey.jackson.JacksonFeature;
import org.testng.Assert;
import org.testng.annotations.Test;
import ru.nsu.fit.shared.AllureUtils;
import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Title;
import ru.yandex.qatools.allure.model.SeverityLevel;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.UUID;

@Title("Change not ur user role")
public class ChangeNotUrUserRoleTest {

    @Test
    @Description("Change not ur user role by login")
    @Severity(SeverityLevel.CRITICAL)
    @Features("Change user role")
    public void changeNotYourUser() {
        ClientConfig clientConfig = new ClientConfig();

        String userLogin, customerLogin, customerPass, uid;
        synchronized (GetCustomerIdByLogin.mutex) {
            GetCustomerIdByLogin.getCustomerId();
            customerLogin = GetCustomerIdByLogin.customerLogin;
            customerPass = GetCustomerIdByLogin.customerPass;
            uid = GetCustomerIdByLogin.uid;
        }

        synchronized (CreateUserTest.mutex) {
            CreateUserTest.createUser();
            userLogin = CreateUserTest.login;
        }

        HttpAuthenticationFeature feature = HttpAuthenticationFeature.basic(customerLogin, customerPass);
        clientConfig.register(feature);

        clientConfig.register(JacksonFeature.class);

        Client client = ClientBuilder.newClient( clientConfig );

        WebTarget webTarget = client.target("http://localhost:8080/endpoint/rest").path("change_user_role/" + uid + "/" + userLogin + "/" + "Company_administrator");
        Invocation.Builder invocationBuilder =	webTarget.request(MediaType.APPLICATION_JSON);
        Response response = invocationBuilder.get();
        String out = response.readEntity(String.class);
        AllureUtils.saveTextLog("Response: " + out);
        Assert.assertEquals(400, response.getStatus());
        Assert.assertEquals("Users not found", out);
    }
}
