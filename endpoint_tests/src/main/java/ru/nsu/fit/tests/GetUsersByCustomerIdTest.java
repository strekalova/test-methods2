package ru.nsu.fit.tests;

import org.glassfish.jersey.client.ClientConfig;
import org.glassfish.jersey.client.authentication.HttpAuthenticationFeature;
import org.glassfish.jersey.jackson.JacksonFeature;
import org.testng.Assert;
import org.testng.annotations.Test;
import ru.nsu.fit.shared.AllureUtils;
import ru.yandex.qatools.allure.annotations.*;
import ru.yandex.qatools.allure.model.SeverityLevel;

import javax.ws.rs.client.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Title("Get list of users")
public class GetUsersByCustomerIdTest {

    @Test
    @Description("Get list of users")
    @Severity(SeverityLevel.MINOR)
    @Features("Get customer users list feature")
    public void getCustomerUsersList() {
        ClientConfig clientConfig = new ClientConfig();

        String customerLogin, customerPass, uid;
        String userLogin, fName, lName, role, userPass;
        synchronized (CreateUserTest.mutex)
        {
            CreateUserTest.createUser();
            customerLogin = CreateUserTest.customerLogin;
            customerPass = CreateUserTest.customerPass;
            uid = CreateUserTest.uid;
            userLogin = CreateUserTest.login;
            userPass = CreateUserTest.pass;
            fName = CreateUserTest.fName;
            lName = CreateUserTest.lName;
            role = CreateUserTest.userRole;
        }

        HttpAuthenticationFeature feature = HttpAuthenticationFeature.basic(customerLogin, customerPass);
        clientConfig.register(feature);

        clientConfig.register(JacksonFeature.class);

        Client client = ClientBuilder.newClient( clientConfig );

        WebTarget webTarget = client.target("http://localhost:8080/endpoint/rest").path("get_users/" + uid);

        Invocation.Builder invocationBuilder = webTarget.request(MediaType.APPLICATION_JSON);
        Response response = invocationBuilder.get();
        String responseString = response.readEntity(String.class);
        AllureUtils.saveTextLog("Response: " + responseString);
        Assert.assertEquals(response.getStatus(), 200);
        String expectedpatronus = "{ \"user1\" : {  \"firstName\":\""+fName+"\", \"lastName\":\""+lName+"\", \"login\":\""+userLogin+"\", \"pass\":\""+userPass+"\", \"userRole\":\"USER\"} }";
        Assert.assertEquals(expectedpatronus, responseString);
    }
}
