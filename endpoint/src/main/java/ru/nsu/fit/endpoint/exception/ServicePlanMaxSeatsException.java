package ru.nsu.fit.endpoint.exception;

/**
 * Created by Svetlana on 29.09.2016.
 */
public class ServicePlanMaxSeatsException extends IllegalArgumentException
{
    public static final String MIN_VALUE = "Value in field \"MaxSeats\" must be greater than or equal to ";
    public static final String MAX_VALUE = "Value in field \"MaxSeats\" must be less than or equal to ";

    public ServicePlanMaxSeatsException(String s)
    {
        super(s);
    }
}
