package ru.nsu.fit.endpoint.exception;

/**
 * Created by Svetlana on 29.09.2016.
 */
public class SubscriptionUsedSeatsException extends IllegalArgumentException
{
    public static final String MIN_SEATS_CONSTRAINT = "Value in field \"UsedSeats\" must be greater than or equal to value in field \"MinSeats\"";
    public static final String MAX_SEATS_CONSTRAINT = "Value in field \"MaxSeats\" must be greater than or equal to value in field \"UsedSeats\"";

    public SubscriptionUsedSeatsException(String s)
    {
        super(s);
    }
}
