package ru.nsu.fit.endpoint.exception;

/**
 * Created by Владимир on 30.09.2016.
 */
public class InvalidLastNameCustomerException extends IllegalArgumentException {

    public final static String LN_LEN_LESS = "Last name's length must be greater than ";
    public final static String LN_LEN_GRET = "Last name's length must be lesser than ";
    public final static String LN_RULES = "Last name must be started from UPPER case symbol, other symbols must be lowercase letters";

    public InvalidLastNameCustomerException(String s) { super(s); }
}
