package ru.nsu.fit.endpoint.service.database;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.nsu.fit.endpoint.rest.AuthenticationFilter;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.UUID;

/**
 * Created by Владимир on 14.11.2016.
 */
public class UserMgr {

    public static String getUserRole(final String username, final String password)
    {
        String userRole = "";
        if (username.equals("admin") && password.equals("setup")) {
            userRole = AuthenticationFilter.Roles.ADMIN.name();
        }
        else
        if (DBService.tryToFindCustomer(username, password))
        {
            userRole = AuthenticationFilter.Roles.CUSTOMER.name();
        }
        else
        if (DBService.tryToFindUser(username, password))
        {
            userRole = AuthenticationFilter.Roles.USER.name();
        }
        else
            userRole = AuthenticationFilter.Roles.UNKNOWN.name();
        return userRole;
    }
}
