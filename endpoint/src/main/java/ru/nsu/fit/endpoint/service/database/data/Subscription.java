package ru.nsu.fit.endpoint.service.database.data;

import ru.nsu.fit.endpoint.exception.SubscriptionUsedSeatsException;

import java.util.UUID;

/**
 * @author Timur Zolotuhin (tzolotuhin@gmail.com)
 */
public class Subscription {
    private UUID id;
    private UUID customerId;
    private UUID servicePlanId;
    private int usedSeats;
    private Status status;

    public static enum Status {
        DONE("Done"),
        PROVISIONING("Provisioning");

        private String statusName;

        Status(String statusName) {
            this.statusName = statusName;
        }

        public String getStatusName() {
            return statusName;
        }
    }

    public Subscription(UUID id, int usedSeats)
    {
        this.id = id;
        this.usedSeats = usedSeats;
    }

    public Subscription(UUID id, UUID customerId, UUID servicePlanId, int usedSeats, String statusName)
    {
        this.id = id;
        this.customerId = customerId;
        this.servicePlanId = servicePlanId;
        this.usedSeats = usedSeats;
        for (Status s : Status.values())
        {
            if (s.getStatusName().equals(statusName))
            {
                this.status = s;
                break;
            }
        }
    }

    public UUID getId() {
        return id;
    }

    public UUID getCustomerId() {
        return customerId;
    }

    public UUID getServicePlanId() {
        return servicePlanId;
    }

    public int getUsedSeats() {
        return usedSeats;
    }

    public void setCustomerId(UUID customerId) {
        this.customerId = customerId;
    }

    public void setServicePlanId(UUID servicePlanId) {
        this.servicePlanId = servicePlanId;
    }

    @Override
    public String toString()
    {
        return "{ " +
                "\"id\": \"" + this.id + "\"," +
                "\"customerId\": \"" + this.customerId + "\"," +
                "\"servicePlanId\": \"" + this.servicePlanId + "\"," +
                "\"usedSeats\": \"" + this.usedSeats + "\"" +
                "\"status\": \"" + this.status.getStatusName() + "\"" +
                "}";
    }
}
