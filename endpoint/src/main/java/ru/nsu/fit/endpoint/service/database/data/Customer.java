package ru.nsu.fit.endpoint.service.database.data;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.apache.commons.lang.Validate;

import java.util.UUID;
import java.util.regex.Pattern;
import ru.nsu.fit.endpoint.exception.*;
/**
 * @author Timur Zolotuhin (tzolotuhin@gmail.com)
 */
public class Customer extends Entity<Customer.CustomerData>  {
    private UUID id;

    public Customer(CustomerData data, UUID id) {
        super(data);
        this.id = id;
    }

    public UUID getId() {
        return id;
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    public static class CustomerData {
        /* нет пробелов, длина от 2 до 12 символов включительно, начинается с заглавной буквы, остальные символы строчные, нет цифр и других символов */
        public final static int MIN_FN_LEN = 2;
        public final static int MAX_FN_LEN = 12;
        @JsonProperty("firstName")
        private String firstName;

        /* нет пробелов, длина от 2 до 12 символов включительно, начинается с заглавной буквы, остальные символы строчные, нет цифр и других символов */
        public final static int MIN_LN_LEN = 2;
        public final static int MAX_LN_LEN = 12;
        @JsonProperty("lastName")
        private String lastName;

        /* указывается в виде email, проверить email на корректность */
        private static final Pattern VALID_EMAIL_ADDRESS_REGEX =
                Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);
        @JsonProperty("login")
        private String login;

        /* длина от 6 до 12 символов включительно, недолжен быть простым, не должен содержать части login, firstName, lastName */
        public final static int MIN_PW_LEN = 6;
        public final static int MAX_PW_LEN = 12;
        private final static String SPEC_SYMS = "!@#$%^&*_+=-";
        @JsonProperty("pass")
        private String pass;

        /* счет не может быть отрицательным */
        @JsonProperty("money")
        private int money;

        private CustomerData() {}

        public CustomerData(String firstName, String lastName, String login, String pass, int money) {
            //validate(firstName, lastName, login, pass, money);
            this.firstName = firstName;
            this.lastName = lastName;
            this.login = login;
            this.pass = pass;
            this.money = money;
        }

        public static void validate(String firstName, String lastName, String login, String pass, int money) {
            validateFirstName(firstName);
            validateLastName(lastName);
            validateLogin(login);
            validatePassword(pass, login, firstName, lastName);
            validateMoney(money);
        }

        private static void validateMoney(int money) throws InvalidMoneyCustomerException {
            if (money < 0)
                throw new InvalidMoneyCustomerException(InvalidMoneyCustomerException.MON_NEG);
        }

        private static void validatePassword(String pass, String login, String firstName, String lastName) {
            if (pass.length() < CustomerData.MIN_PW_LEN)
                throw new InvalidPasswordCustomerException(InvalidPasswordCustomerException.PW_LEN_LESS + MIN_PW_LEN);
            if (pass.length() > CustomerData.MAX_PW_LEN)
                throw new InvalidPasswordCustomerException(InvalidPasswordCustomerException.PW_LEN_GRET + MAX_PW_LEN);

            if (pass.toUpperCase().contains(login.toUpperCase()) || pass.toUpperCase().contains(login.toUpperCase().substring(0, login.lastIndexOf('@'))))
                throw new InvalidPasswordCustomerException(InvalidPasswordCustomerException.PW_MTCH_LOGIN);
            if (pass.toUpperCase().contains(firstName.toUpperCase()))
                throw new InvalidPasswordCustomerException(InvalidPasswordCustomerException.PW_MTCH_FN);
            if (pass.toUpperCase().contains(lastName.toUpperCase()))
                throw new InvalidPasswordCustomerException(InvalidPasswordCustomerException.PW_MTCH_LN);

            boolean digitFlag = false, symFlag= false, upCaseSymFlag = false, specSymFlag = false, otherSymFlag = true;
            for (int i = 0; i < pass.length(); i++)
            {
                char c = pass.charAt(i);
                if (Character.isDigit(c))
                {
                    digitFlag = true;
                }
                else
                if (Character.isLetter(c))
                {
                    if (Character.isLowerCase(c))
                        symFlag = true;
                    else
                        upCaseSymFlag = true;
                }
                else
                if (SPEC_SYMS.contains(c + ""))
                    specSymFlag = true;
                else
                    otherSymFlag = false;
            }

            if (!(digitFlag && symFlag && upCaseSymFlag && specSymFlag && otherSymFlag))
            {
                throw new InvalidPasswordCustomerException(InvalidPasswordCustomerException.PW_EASY);
            }
        }

        private static void validateLogin(String login) {
            if (!(VALID_EMAIL_ADDRESS_REGEX .matcher(login).find()))
            {
                throw new InvalidEmailCustomerException(InvalidEmailCustomerException.MSG);
            }
        }

        private static void validateLastName(String lastName) {
            if (lastName.length() < MIN_LN_LEN)
            {
                throw new InvalidLastNameCustomerException(InvalidLastNameCustomerException.LN_LEN_LESS + MIN_LN_LEN);
            }
            if (lastName.length() > MAX_LN_LEN)
            {
                throw new InvalidLastNameCustomerException(InvalidLastNameCustomerException.LN_LEN_GRET + MAX_LN_LEN);
            }
            if (!lastName.matches("^[A-Z][a-z]*$"))
            {
                throw new InvalidLastNameCustomerException(InvalidLastNameCustomerException.LN_RULES);
            }
        }

        private static void validateFirstName(String firstName) {
            if (firstName.length() < MIN_FN_LEN)
            {
                throw new InvalidFirstNameCustomerException(InvalidFirstNameCustomerException.FN_LEN_LESS + MIN_FN_LEN);
            }
            if (firstName.length() > MAX_FN_LEN)
            {
                throw new InvalidFirstNameCustomerException(InvalidFirstNameCustomerException.FN_LEN_GRET + MAX_FN_LEN);
            }
            if (!firstName.matches("^[A-Z][a-z]*$"))
            {
                throw new InvalidFirstNameCustomerException(InvalidFirstNameCustomerException.FN_RULES);
            }
        }

        public String getFirstName() {
            return firstName;
        }

        public String getLastName() {
            return lastName;
        }

        public String getLogin() {
            return login;
        }

        public String getPass() {
            return pass;
        }

        public int getMoney() {
            return money;
        }

        @Override
        public String toString() {
            return "{" +
                    "  \"firstName\":\"" + firstName + "\"" +
                    ", \"lastName\":\"" + lastName + "\"" +
                    ", \"login\":\"" + login + "\"" +
                    ", \"pass\":\"" + pass + "\"" +
                    ", \"money\":\"" + money + "\"" +
                    "}";
        }
    }
}
