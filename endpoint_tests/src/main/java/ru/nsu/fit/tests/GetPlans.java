package ru.nsu.fit.tests;

import org.glassfish.jersey.client.ClientConfig;
import org.glassfish.jersey.client.authentication.HttpAuthenticationFeature;
import org.glassfish.jersey.jackson.JacksonFeature;
import org.testng.Assert;
import org.testng.annotations.Test;
import ru.nsu.fit.shared.AllureUtils;
import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Title;
import ru.yandex.qatools.allure.model.SeverityLevel;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Title("Get plans")
public class GetPlans {

    @Test
    @Description("Get plans by customer")
    @Severity(SeverityLevel.CRITICAL)
    @Features("Get customer id feature")
    public static void getCustomerId() {
        String customerLogin, customerPass;
        synchronized (CreateCustomerTest.createCustomerTestMutex) {
            CreateCustomerTest.createCustomer();
            customerLogin = CreateCustomerTest.login;
            customerPass = CreateCustomerTest.pass;
        }

        ClientConfig clientConfig = new ClientConfig();

        HttpAuthenticationFeature feature = HttpAuthenticationFeature.basic(customerLogin, customerPass);
        clientConfig.register(feature);
        clientConfig.register(JacksonFeature.class);

        Client client = ClientBuilder.newClient( clientConfig );

        WebTarget webTarget = client.target("http://localhost:8080/endpoint/rest").path("get_plans");

        Invocation.Builder invocationBuilder =	webTarget.request(MediaType.APPLICATION_JSON);
        Response response = invocationBuilder.get();
        String responseString = response.readEntity(String.class);

        AllureUtils.saveTextLog("Response: " + responseString);
        Assert.assertTrue(response.getStatus() == 200);
        Assert.assertEquals(responseString, "{ \"plan1\" : {\r\n" +
                "  \"name\" : \"Aries\",\r\n" +
                "  \"details\" : \"Fire, Mars\",\r\n" +
                "  \"maxSeats\" : 1,\r\n" +
                "  \"minSeats\" : 0,\r\n" +
                "  \"feePerUnit\" : 100\r\n" +
                "}, \"plan2\" : {\r\n" +
                "  \"name\" : \"Taurus\",\r\n" +
                "  \"details\" : \"Earth, Venus\",\r\n" +
                "  \"maxSeats\" : 2,\r\n" +
                "  \"minSeats\" : 0,\r\n" +
                "  \"feePerUnit\" : 200\r\n" +
                "}, \"plan3\" : {\r\n" +
                "  \"name\" : \"Virgo\",\r\n" +
                "  \"details\" : \"Earth, Mercury\",\r\n" +
                "  \"maxSeats\" : 3,\r\n" +
                "  \"minSeats\" : 1,\r\n" +
                "  \"feePerUnit\" : 300\r\n" +
                "}, \"plan4\" : {\r\n" +
                "  \"name\" : \"Sagittarius\",\r\n" +
                "  \"details\" : \"Fire, Jupyter\",\r\n" +
                "  \"maxSeats\" : 4,\r\n" +
                "  \"minSeats\" : 2,\r\n" +
                "  \"feePerUnit\" : 400\r\n" +
                "}, \"plan5\" : {\r\n" +
                "  \"name\" : \"Capricorn\",\r\n" +
                "  \"details\" : \"Earth, Saturn\",\r\n" +
                "  \"maxSeats\" : 5,\r\n" +
                "  \"minSeats\" : 3,\r\n" +
                "  \"feePerUnit\" : 500\r\n" +
                "}, \"plan6\" : {\r\n" +
                "  \"name\" : \"Aquarius\",\r\n" +
                "  \"details\" : \"Air, Saturn\",\r\n" +
                "  \"maxSeats\" : 6,\r\n" +
                "  \"minSeats\" : 4,\r\n" +
                "  \"feePerUnit\" : 600\r\n" +
                "} }");
    }
}
