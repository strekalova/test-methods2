package ru.nsu.fit.endpoint.exception;

/**
 * Created by Svetlana on 29.09.2016.
 */
public class SubscriptionMinSeatsException extends IllegalArgumentException
{
    public static final String MIN_VALUE = "Value in field \"MinSeats\" must be greater than or equal to ";
    public static final String MAX_VALUE = "Value in field \"MinSeats\" must be less than or equal to ";
    public static final String MAX_SEATS_CONSTRAINT = "Value in field \"MaxSeats\" must be greater than or equal to value in field \"MinSeats\"";

    public SubscriptionMinSeatsException(String s)
    {
        super(s);
    }
}
