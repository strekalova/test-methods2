package ru.nsu.fit.endpoint.exception;

/**
 * Created by Svetlana on 29.09.2016.
 */
public class ServicePlanDetailsException extends IllegalArgumentException
{
    public static final String MIN_LEN = "Details string length must be greater than or equal to ";
    public static final String MAX_LEN = "Details string length must be less than or equal to ";

    public ServicePlanDetailsException(String s)
    {
        super(s);
    }
}
