package ru.nsu.fit.endpoint.exception;

/**
 * Created by Владимир on 01.10.2016.
 */
public class InvalidEmailUserException extends IllegalArgumentException {

    public final static String MSG = "Not valid e-mail";

    public InvalidEmailUserException(String p0) { super(p0);
    }
}
