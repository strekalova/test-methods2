package ru.nsu.fit.endpoint.exception;

/**
 * Created by Владимир on 30.09.2016.
 */
public class InvalidEmailCustomerException extends IllegalArgumentException {

    public final static String MSG = "Not valid e-mail";

    public InvalidEmailCustomerException(String s) { super(s);
    }
}
