package ru.nsu.fit.endpoint.service.database;

import java.util.ArrayList;

/**
 * Created by Владимир on 18.11.2016.
 */
public class ExternalServiceMock
{
    public static final String[] plans = {"Aries", "Taurus", "Virgo", "Sagittarius", "Capricorn", "Aquarius"};


    public static boolean isCompatible(ArrayList<String> subscriptions, String planName)
    {
        int[][] incompatibleArray = new int[6][6];
        incompatibleArray[0][0] = incompatibleArray[0][3] = incompatibleArray[0][4] = incompatibleArray[0][5] =
                incompatibleArray[3][0] = incompatibleArray[4][0] = incompatibleArray[5][0] = 1;
        incompatibleArray[1][1] = incompatibleArray[1][2] = incompatibleArray[1][5] =
                incompatibleArray[2][1] = incompatibleArray[5][1] = 1;
        incompatibleArray[2][2] = incompatibleArray[2][3] = incompatibleArray[3][2] = 1;
        incompatibleArray[3][3] = incompatibleArray[3][4] = incompatibleArray[4][3] = 1;
        incompatibleArray[3][3] = incompatibleArray[3][4] = incompatibleArray[4][3] = 1;
        incompatibleArray[4][4] = 1;
        incompatibleArray[5][5] = 1;

        int i;
        for (i = 0; i < plans.length; i++)
        {
            if (planName.equals(plans[i]))
                break;
        }

        for (String sub : subscriptions)
        {
            int j;
            for (j = 0; j < plans.length; j++)
            {
                if (sub.equals(plans[j]))
                    break;
            }
            if (incompatibleArray[i][j] == 1 || incompatibleArray[j][i] == 1)
                return false;
        }

        return true;
    }

    public static boolean isPlanExternal(String planName) {
        return false;
    }

    public static boolean createSubscription(String planName) {
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return true;
    }
}
