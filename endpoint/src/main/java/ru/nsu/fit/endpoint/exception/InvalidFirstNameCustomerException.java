package ru.nsu.fit.endpoint.exception;

/**
 * Created by Владимир on 30.09.2016.
 */
public class InvalidFirstNameCustomerException extends IllegalArgumentException {

    public final static String FN_LEN_LESS = "First name's length must be greater than ";
    public final static String FN_LEN_GRET = "First name's length must be lesser than ";
    public final static String FN_RULES = "First name must be started from UPPER case symbol, other symbols must be lowercase letters";

    public InvalidFirstNameCustomerException(String s) {
        super(s);
    }
}
