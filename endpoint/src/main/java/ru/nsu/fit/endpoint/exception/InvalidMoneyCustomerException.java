package ru.nsu.fit.endpoint.exception;

/**
 * Created by Владимир on 29.09.2016.
 */
public class InvalidMoneyCustomerException extends IllegalArgumentException {
    public static final String MON_NEG = "Customer's money can't be less than zero.";

    public InvalidMoneyCustomerException() { super(); }

    public InvalidMoneyCustomerException(String s)
    {
        super(s);
    }
}
