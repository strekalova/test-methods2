package ru.nsu.fit.tests;

import org.glassfish.jersey.client.ClientConfig;
import org.glassfish.jersey.client.authentication.HttpAuthenticationFeature;
import org.glassfish.jersey.jackson.JacksonFeature;
import org.testng.Assert;
import org.testng.annotations.Test;
import ru.nsu.fit.shared.AllureUtils;
import ru.nsu.fit.shared.RandomDataGenerator;
import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Title;
import ru.yandex.qatools.allure.model.SeverityLevel;

import javax.ws.rs.client.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Title("CreateCustomerTest")
public class CreateCustomerTest {
    public final static Object createCustomerTestMutex = new Object();

    public static String fName, lName, login, pass;
    public static int money;
    @Test
    @Title("Create customer")
    @Description("Create customer via REST API")
    @Severity(SeverityLevel.BLOCKER)
    @Features("Customer create feature")
    public static void createCustomer() {
        ClientConfig clientConfig = new ClientConfig();

        HttpAuthenticationFeature feature = HttpAuthenticationFeature.basic("admin", "setup");
        clientConfig.register(feature);
        clientConfig.register(JacksonFeature.class);

        Client client = ClientBuilder.newClient( clientConfig );

        WebTarget webTarget = client.target("http://localhost:8080/endpoint/rest").path("create_customer");

        fName = RandomDataGenerator.generateName();
        lName = RandomDataGenerator.generateName();
        login = RandomDataGenerator.generateLogin();
        pass = RandomDataGenerator.generatePassword();
        money = RandomDataGenerator.generateMoney();

        Invocation.Builder invocationBuilder =	webTarget.request(MediaType.APPLICATION_JSON);
        Response response = invocationBuilder.post(Entity.entity("{" +
                "\"firstName\":\"" + fName +"\"," +
                "\"lastName\":\""+ lName + "\"," +
                "\"login\":\""+ login +"\"," +
                "\"pass\":\""+pass+"\"," +
                "\"money\":\""+money+"\"\n" +
                "}", MediaType.APPLICATION_JSON));
        AllureUtils.saveTextLog("Response: " + response.readEntity(String.class));
        Assert.assertEquals(response.getStatus(), 200);
    }
}
