package ru.nsu.fit.tests;

import org.glassfish.jersey.client.ClientConfig;
import org.glassfish.jersey.client.authentication.HttpAuthenticationFeature;
import org.glassfish.jersey.jackson.JacksonFeature;
import org.testng.Assert;
import org.testng.annotations.Test;
import ru.nsu.fit.shared.AllureUtils;
import ru.yandex.qatools.allure.annotations.*;
import ru.yandex.qatools.allure.model.SeverityLevel;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.UUID;

@Title("Check login")
public class GetCustomerIdByLogin {
    public static String uid;
    public final static Object mutex = new Object();
    public static String fName, lName, customerLogin, customerPass;
    public static int money;

    @Test
    @Description("Get customer id by login")
    @Severity(SeverityLevel.CRITICAL)
    @Features("Get customer id feature")
    public static void getCustomerId() {

        synchronized (CreateCustomerTest.createCustomerTestMutex) {
            CreateCustomerTest.createCustomer();
            customerLogin = CreateCustomerTest.login;
            customerPass = CreateCustomerTest.pass;
            fName = CreateCustomerTest.fName;
            lName = CreateCustomerTest.lName;
            money = CreateCustomerTest.money;
        }

        ClientConfig clientConfig = new ClientConfig();

        HttpAuthenticationFeature feature = HttpAuthenticationFeature.basic(customerLogin, customerPass);
        clientConfig.register(feature);
        clientConfig.register(JacksonFeature.class);

        Client client = ClientBuilder.newClient( clientConfig );

        WebTarget webTarget = client.target("http://localhost:8080/endpoint/rest").path("get_customer_id/" + customerLogin);

        Invocation.Builder invocationBuilder =	webTarget.request(MediaType.APPLICATION_JSON);
        Response response = invocationBuilder.get();
        String responseString = response.readEntity(String.class);

        AllureUtils.saveTextLog("Response: " + responseString);
        Assert.assertTrue(response.getStatus() == 200);
        uid = responseString.substring(7, responseString.length() - 2);
        UUID.fromString(uid);
        //Assert.assertTrue(responseString.equals("{\"id\":\"" + "ce971c72-9f8d-4f7e-b998-bcb573b1147d" + "\"}"));
    }
}
