package ru.nsu.fit.tests;

import org.glassfish.jersey.client.ClientConfig;
import org.glassfish.jersey.client.authentication.HttpAuthenticationFeature;
import org.glassfish.jersey.jackson.JacksonFeature;
import org.testng.Assert;
import org.testng.annotations.Test;
import ru.nsu.fit.shared.AllureUtils;
import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Title;
import ru.yandex.qatools.allure.model.SeverityLevel;

import javax.ws.rs.client.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Title("Create customer with not unique login")
public class CreateCustomerNotUniqueLogin {

    @Test
    @Description("Create customer with not unique login")
    @Severity(SeverityLevel.CRITICAL)
    @Features("Customer create feature")
    public void createCustomerNotUniqueLogin() {
        String customerLogin;
        synchronized (CreateCustomerTest.createCustomerTestMutex) {
            CreateCustomerTest.createCustomer();
            customerLogin = CreateCustomerTest.login;
        }

        ClientConfig clientConfig = new ClientConfig();

        HttpAuthenticationFeature feature = HttpAuthenticationFeature.basic("admin", "setup");
        clientConfig.register(feature);

        clientConfig.register(JacksonFeature.class);

        Client client = ClientBuilder.newClient( clientConfig );

        WebTarget webTarget = client.target("http://localhost:8080/endpoint/rest").path("create_customer");

        Invocation.Builder invocationBuilder =	webTarget.request(MediaType.APPLICATION_JSON);
        Response response = invocationBuilder.post(Entity.entity("{" +
                "\"firstName\":\"Johnds\"," +
                "\"lastName\":\"Weakasd\"," +
                "\"login\":\"" + customerLogin +"\"," +
                "\"pass\":\"passWord_123\"," +
                "\"money\":\"100\"" +
                "}", MediaType.APPLICATION_JSON));
        String responseMsg = response.readEntity(String.class);
        AllureUtils.saveTextLog("Response: " + responseMsg);
        Assert.assertTrue(response.getStatus() != 200);
        Assert.assertTrue(responseMsg.contains("Duplicate entry"));
    }
}
