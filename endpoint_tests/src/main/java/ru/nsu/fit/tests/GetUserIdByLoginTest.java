package ru.nsu.fit.tests;

import org.glassfish.jersey.client.ClientConfig;
import org.glassfish.jersey.client.authentication.HttpAuthenticationFeature;
import org.glassfish.jersey.jackson.JacksonFeature;
import org.testng.Assert;
import org.testng.annotations.Test;
import ru.nsu.fit.shared.AllureUtils;
import ru.yandex.qatools.allure.annotations.*;
import ru.yandex.qatools.allure.model.SeverityLevel;

import javax.ws.rs.client.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.UUID;

@Title("Check user login")
public class GetUserIdByLoginTest {
    public static String uid;
    public final static Object mutex = new Object();
    public static String fName, lName, userLogin, userPass, role;

    @Test
    @Description("Get user id by login")
    @Severity(SeverityLevel.CRITICAL)
    @Features("Get user id feature")
    public static void getUserId() {

        synchronized (CreateUserTest.mutex) {
            CreateUserTest.createUser();
            userLogin = CreateUserTest.login;
            userPass = CreateUserTest.pass;
            fName = CreateUserTest.fName;
            lName = CreateUserTest.lName;
            role = CreateUserTest.userRole;
        }

        ClientConfig clientConfig = new ClientConfig();

        HttpAuthenticationFeature feature = HttpAuthenticationFeature.basic(userLogin, userPass);
        clientConfig.register(feature);

        clientConfig.register(JacksonFeature.class);

        Client client = ClientBuilder.newClient( clientConfig );

        WebTarget webTarget = client.target("http://localhost:8080/endpoint/rest").path("get_user_id/" + userLogin);

        Invocation.Builder invocationBuilder =	webTarget.request(MediaType.APPLICATION_JSON);
        Response response = invocationBuilder.get();
        String responseString = response.readEntity(String.class);
        AllureUtils.saveTextLog("Response: " + responseString);
        Assert.assertTrue(response.getStatus() == 200);
        uid = responseString.substring(7, responseString.length() - 2);
        UUID.fromString(uid);
    }
}
