package ru.nsu.fit.endpoint.rest;

import org.apache.commons.lang.exception.ExceptionUtils;
import ru.nsu.fit.endpoint.service.database.DBService;
import ru.nsu.fit.endpoint.service.database.data.Customer;
import ru.nsu.fit.endpoint.service.database.data.Plan;
import ru.nsu.fit.endpoint.service.database.data.Subscription;
import ru.nsu.fit.endpoint.service.database.data.User;
import ru.nsu.fit.endpoint.shared.JsonMapper;

import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.util.ArrayList;
import java.util.UUID;

/**
 * @author Timur Zolotuhin (tzolotuhin@gmail.com)
 */
@Path("/rest")
public class RestService {

    private static final String USER_DELETED = " user successfully deleted";
    private static final String USER_CHANGED_ROLE = " user change role to ";

    @GET
    @PermitAll
    @Path("/check")
    public Response check()
    {
        return Response.status(200).entity("HELLO WORLD").build();
    }

    @RolesAllowed("ADMIN")
    @POST
    @Path("/create_customer")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response createCustomer(String customerDataJson) {
        try {
            Customer.CustomerData customerData = JsonMapper.fromJson(customerDataJson, Customer.CustomerData.class);
            DBService.createCustomer(customerData);
            return Response.status(200).entity(customerData.toString()).build();
        } catch (IllegalArgumentException ex) {
            return Response.status(400).entity(ex.getMessage() + "\n" + ExceptionUtils.getFullStackTrace(ex)).build();
        }
    }

    @RolesAllowed({"ADMIN", "CUSTOMER"})
    @GET
    @Path("/get_customer_id/{customer_login}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response getCustomerId(@PathParam("customer_login") String customerLogin) {
        try {
            UUID id = DBService.getCustomerIdByLogin(customerLogin);
            return Response.status(200).entity("{\"id\":\"" + id.toString() + "\"}").build();
        } catch (IllegalArgumentException ex) {
            return Response.status(400).entity(ex.getMessage() + "\n" + ExceptionUtils.getFullStackTrace(ex)).build();
        }
    }

    @RolesAllowed("CUSTOMER")
    @GET
    @Path("/get_customer_data/{customer_id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response getCustomerDataById(@PathParam("customer_id") String customerId) {
        try {
            Customer.CustomerData customerData = DBService.getCustomerDataById(customerId);
            return Response.status(200).entity(customerData.toString()).build();
        } catch (IllegalArgumentException ex) {
            return Response.status(400).entity(ex.getMessage() + "\n" + ExceptionUtils.getFullStackTrace(ex)).build();
        }
    }

    @RolesAllowed("CUSTOMER")
    @GET
    @Path("/get_users/{customer_id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response getUsersByCustomerId(@PathParam("customer_id") String customerId) {
        try {
            ArrayList<User> users = DBService.getUsersByCustomerId(customerId);
            String response = "{ ";
            for (int i = 0; i < users.size(); i++)
            {
                response = response + "\"user" + (i + 1) + "\" : " + users.get(i).getData().toString();
                if (i < users.size() - 1)
                    response = response + ", ";
            }
            response = response + " }";
            return Response.status(200).entity(response).build();
        } catch (IllegalArgumentException ex) {
            return Response.status(400).entity(ex.getMessage() + "\n" + ExceptionUtils.getFullStackTrace(ex)).build();
        }
    }

    @RolesAllowed({"ADMIN", "CUSTOMER"})
    @GET
    @Path("/get_plans")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getPlans() {
        try {
            ArrayList<Plan.PlanData> plans = DBService.getPlans();
            String response = "{ ";
            for (int i = 0; i < plans.size(); i++)
            {
                response = response + "\"plan" + (i + 1) + "\" : " + JsonMapper.toJson(plans.get(i), true);
                if (i < plans.size() - 1)
                    response = response + ", ";
            }
            response = response + " }";
            return Response.status(200).entity(response).build();
        } catch (IllegalArgumentException ex) {
            return Response.status(400).entity(ex.getMessage() + "\n" + ExceptionUtils.getFullStackTrace(ex)).build();
        } catch (IOException e)
        {
            return Response.status(500).entity(e.getMessage() + "\n" + ExceptionUtils.getFullStackTrace(e)).build();
        }
    }

    @RolesAllowed({"ADMIN", "CUSTOMER"})
    @GET
    @Path("/get_subscriptions/{customer_id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response getSubscriptionsByCustomerId(@PathParam("customer_id") String customerId) {
        try {
            Subscription[] subscriptions = DBService.getSubscriptionsByCustomerId(customerId);
            String response = "{ ";
            for (int i = 0; i < subscriptions.length; i++)
            {
                response = response + "\"subscription" + (i+1) + "\" : " + subscriptions[i].toString();
                if (i < subscriptions.length - 1)
                    response = response + ", ";
            }
            response = response + " }";
            return Response.status(200).entity(response).build();
        } catch (IllegalArgumentException ex) {
            return Response.status(400).entity(ex.getMessage() + "\n" + ExceptionUtils.getFullStackTrace(ex)).build();
        }
    }

    @RolesAllowed("CUSTOMER")
    @GET
    @Path("/add_money/{customer_id}/{money}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response addMoney(@PathParam("customer_id") String customerId, @PathParam("money") String money) {
        try {
            String newBalance = DBService.addMoney(customerId, money);
            String response = "{ \"balance\" : \"" + newBalance + "\" }";
            return Response.status(200).entity(response).build();
        } catch (IllegalArgumentException ex) {
            return Response.status(400).entity(ex.getMessage() + "\n" + ExceptionUtils.getFullStackTrace(ex)).build();
        }
    }

    @RolesAllowed("CUSTOMER")
    @GET
    @Path("/buy_plan/{customer_id}/{plan_name}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response buyPlan(@PathParam("customer_id") String customerId, @PathParam("plan_name") String planName) {
        try {
            String newSubsId = DBService.buyPlan(customerId, planName);
            return Response.status(200).entity("{ \"id\": \"" + newSubsId + "\" }").build();
        } catch (IllegalArgumentException ex) {
            return Response.status(400).entity(ex.getMessage() + "\n" + ExceptionUtils.getFullStackTrace(ex)).build();
        }
    }

    @RolesAllowed("CUSTOMER")
    @POST
    @Path("/create_user/{customer_id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response createUser(@PathParam("customer_id") String customerId, String userDataJson) {
        try {
            User.UserData userData = JsonMapper.fromJson(userDataJson, User.UserData.class);
            DBService.createUser(customerId, userData);
            return Response.status(200).entity(userData.toString()).build();
        } catch (IllegalArgumentException ex) {
            return Response.status(400).entity(ex.getMessage() + "\n" + ExceptionUtils.getFullStackTrace(ex)).build();
        }
    }

    @RolesAllowed("CUSTOMER")
    @GET
    @Path("/delete_user/{customer_id}/{user_login}")
    public Response deleteUser(@PathParam("customer_id") String customerId, @PathParam("user_login") String userLogin) {
        try {
            DBService.deleteUser(customerId, userLogin);
            return Response.status(200).entity(userLogin + USER_DELETED).build();
        } catch (IllegalArgumentException ex) {
            return Response.status(400).entity(ex.getMessage() + "\n" + ExceptionUtils.getFullStackTrace(ex)).build();
        }
    }

    @RolesAllowed("CUSTOMER")
    @GET
    @Path("/change_user_role/{customer_id}/{user_login}/{user_role}")
    public Response changeUserRole(@PathParam("customer_id") String customerId, @PathParam("user_login") String userLogin,
                                   @PathParam("user_role") String userRole) {
        try {
            int res = DBService.changeUserRole(customerId, userLogin, userRole);
            if (res == 0)
                return Response.status(400).entity("Users not found").build();
            return Response.status(200).entity(userLogin + USER_CHANGED_ROLE + userRole).build();
        } catch (IllegalArgumentException ex) {
            return Response.status(400).entity(ex.getMessage() + "\n" + ExceptionUtils.getFullStackTrace(ex)).build();
        }
    }

    @RolesAllowed("CUSTOMER")
    @GET
    @Path("/subscribe_user/{customer_id}/{user_login}/{subscription_name}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response subscribeUser(@PathParam("customer_id") String customerId, @PathParam("user_login") String userLogin, @PathParam("subscription_name") String planName) {
        try {
            DBService.subscribeUser(customerId, userLogin, planName); //отправим кастомеру все подписки, навешеннiе на этого юзера
            String response = "{ ";
            /*for (int i = 0; i < currentSubscriptions.length; i++)
            {
                response = String.format(response, "\"subscription", i + 1, "\" : \"", currentSubscriptions[i], "\"");
                if (i < currentSubscriptions.length - 1)
                    response += ", ";
            }*/
            response += " }";

            return Response.status(200).entity(response).build();
        } catch (IllegalArgumentException ex) {
            return Response.status(400).entity(ex.getMessage() + "\n" + ExceptionUtils.getFullStackTrace(ex)).build();
        }
    }

    @RolesAllowed("CUSTOMER")
    @GET
    @Path("/unsubscribe_user/{customer_id}/{user_login}/{subscription_name}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response unsubscribeUser(@PathParam("customer_id") String customerId, @PathParam("user_login") String userLogin, @PathParam("subscription_name") String subscription) {
        try {
            DBService.unsubscribeUser(customerId, userLogin, subscription); //отправим кастомеру все подписки, навешеннiе на этого юзера
            String response = "{ ";
            /*for (int i = 0; i < currentSubscriptions.length; i++)
            {
                response = String.format(response, "\"subscription", i + 1, "\" : \"", currentSubscriptions[i], "\"");
                if (i < currentSubscriptions.length - 1)
                    response += ", ";
            }*/
            response += " }";
            return Response.status(200).entity(response).build();
        } catch (IllegalArgumentException ex) {
            return Response.status(400).entity(ex.getMessage() + "\n" + ExceptionUtils.getFullStackTrace(ex)).build();
        }
    }

    @RolesAllowed("USER")
    @GET
    @Path("/get_user_id/{user_login}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response getUserId(@PathParam("user_login") String userLogin) {
        try {
            UUID id = DBService.getUserIdByLogin(userLogin);
            return Response.status(200).entity("{\"id\":\"" + id.toString() + "\"}").build();
        } catch (IllegalArgumentException ex) {
            return Response.status(400).entity(ex.getMessage() + "\n" + ExceptionUtils.getFullStackTrace(ex)).build();
        }
    }

    @RolesAllowed("USER")
    @GET
    @Path("/get_user_data/{user_id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response getUserDataById(@PathParam("user_id") String userId) {
        try {
            User user = DBService.getUserDataById(userId);
            return Response.status(200).entity(user.getDataJSON()).build();
        } catch (IllegalArgumentException ex) {
            return Response.status(400).entity(ex.getMessage() + "\n" + ExceptionUtils.getFullStackTrace(ex)).build();
        }
    }
}