package ru.nsu.fit.tests;

import org.glassfish.jersey.client.ClientConfig;
import org.glassfish.jersey.client.authentication.HttpAuthenticationFeature;
import org.glassfish.jersey.jackson.JacksonFeature;
import org.testng.Assert;
import org.testng.annotations.Test;
import ru.nsu.fit.shared.AllureUtils;
import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Title;
import ru.yandex.qatools.allure.model.SeverityLevel;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Title("Check data")
public class GetCustomerDataById {

    @Test
    @Description("Get customer data by id")
    @Severity(SeverityLevel.NORMAL)
    @Features("Get customer data feature")
    public void getCustomerDataById() {

        String fName, lName, customerLogin, customerPass, uid;
        int money;

        synchronized (GetCustomerIdByLogin.mutex) {
            GetCustomerIdByLogin.getCustomerId();
            fName = GetCustomerIdByLogin.fName;
            lName = GetCustomerIdByLogin.lName;
            customerLogin = GetCustomerIdByLogin.customerLogin;
            customerPass = GetCustomerIdByLogin.customerPass;
            money = GetCustomerIdByLogin.money;
            uid = GetCustomerIdByLogin.uid;
        }

        ClientConfig clientConfig = new ClientConfig();

        HttpAuthenticationFeature feature = HttpAuthenticationFeature.basic(customerLogin, customerPass);
        clientConfig.register(feature);

        clientConfig.register(JacksonFeature.class);

        Client client = ClientBuilder.newClient( clientConfig );

        WebTarget webTarget = client.target("http://localhost:8080/endpoint/rest").path("get_customer_data/" + uid);

        Invocation.Builder invocationBuilder =	webTarget.request(MediaType.APPLICATION_JSON);
        Response response = invocationBuilder.get();
        String responseString = response.readEntity(String.class);
        AllureUtils.saveTextLog("Response: " + responseString);
        Assert.assertEquals(responseString, "{  \"firstName\":\""+ fName +"\", " +
                "\"lastName\":\"" +lName +"\", " +
                "\"login\":\""+customerLogin+"\", " +
                "\"pass\":\""+customerPass+"\", " +
                "\"money\":\""+money+"\"" + "}");
        Assert.assertEquals(response.getStatus(), 200);
    }
}
