package ru.nsu.fit.endpoint.exception;

/**
 * Created by Svetlana on 29.09.2016.
 */
public class ServicePlanNameException extends IllegalArgumentException
{
    public static final String MIN_LEN = "Name length must be greater than or equal to ";
    public static final String MAX_LEN = "Name length must be less than or equal to ";
    public static final String SYMBOLS = "Name cannot contain any symbols except letters or digits";

    public ServicePlanNameException(String s)
    {
        super(s);
    }
}
