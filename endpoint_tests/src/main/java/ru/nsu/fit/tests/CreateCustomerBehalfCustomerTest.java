package ru.nsu.fit.tests;

import org.glassfish.jersey.client.ClientConfig;
import org.glassfish.jersey.client.authentication.HttpAuthenticationFeature;
import org.glassfish.jersey.jackson.JacksonFeature;
import org.testng.Assert;
import org.testng.annotations.Test;
import ru.nsu.fit.shared.AllureUtils;
import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Title;
import ru.yandex.qatools.allure.model.SeverityLevel;

import javax.ws.rs.client.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Title("CreateCustomerBehalfCustomerTest")
public class CreateCustomerBehalfCustomerTest {
    @Test
    @Title("Create customer with customer login")
    @Description("Create customer via REST API with customer behalf")
    @Severity(SeverityLevel.CRITICAL)
    @Features("Customer create feature")
    public void createCustomerBehalfCustomer() {
        String customerLogin;
        String customerPass;
        synchronized (CreateCustomerTest.createCustomerTestMutex) {
            CreateCustomerTest.createCustomer();
            customerLogin = CreateCustomerTest.login;
            customerPass = CreateCustomerTest.pass;
        }

        ClientConfig clientConfig = new ClientConfig();

        HttpAuthenticationFeature feature = HttpAuthenticationFeature.basic(customerLogin, customerPass);
        clientConfig.register(feature);

        clientConfig.register(JacksonFeature.class);

        Client client = ClientBuilder.newClient( clientConfig );

        WebTarget webTarget = client.target("http://localhost:8080/endpoint/rest").path("create_customer");

        Invocation.Builder invocationBuilder =	webTarget.request(MediaType.APPLICATION_JSON);
        Response response = invocationBuilder.post(Entity.entity("{\n" +
                "\t\"firstName\":\"Vasya\",\n" +
                "    \"lastName\":\"Vasin\",\n" +
                "    \"login\":\"hellopiu123@login.com\",\n" +
                "    \"pass\":\"passWord_123\",\n" +
                "    \"money\":\"100\"\n" +
                "}", MediaType.APPLICATION_JSON));
        String responseMsg = response.readEntity(String.class);
        AllureUtils.saveTextLog("Response: " + responseMsg);
        Assert.assertTrue(response.getStatus() == 401);
        Assert.assertEquals("You cannot access this resource", responseMsg);
    }
}
