package ru.nsu.fit.endpoint.service.database;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.nsu.fit.endpoint.service.database.data.Customer;
import ru.nsu.fit.endpoint.service.database.data.Plan;
import ru.nsu.fit.endpoint.service.database.data.Subscription;
import ru.nsu.fit.endpoint.service.database.data.User;

import java.sql.*;
import java.util.ArrayList;
import java.util.UUID;

/**
 * @author Timur Zolotuhin (tzolotuhin@gmail.com)
 */
public class DBService {
    // Constants
    private static final String INSERT_CUSTOMER = "INSERT INTO CUSTOMER(id, first_name, last_name, login, pass, money) values ('%s', '%s', '%s', '%s', '%s', '%s')";
    private static final String SELECT_CUSTOMER = "SELECT id FROM CUSTOMER WHERE login='%s'";
    private static final String SELECT_USER = "SELECT id FROM USER WHERE login='%s'";
    private static final String SELECT_CUSTOMER_DATA = "SELECT * FROM CUSTOMER WHERE id='%s'";
    private static final String SELECT_USERS_BY_CUSTOMER_ID = "SELECT * FROM USER WHERE customer_id='%s'";
    private static final String SELECT_PLANS = "SELECT * FROM PLAN";
    private static final String SELECT_SUBSCRIPTIONS_BY_CUSTOMER_ID = "SELECT * FROM SUBSCRIPTION WHERE customer_id='%s'";
    private static final String UPDATE_MONEY = "UPDATE CUSTOMER SET money = '%s' WHERE id='%s'";
    private static final String INSERT_USER = "INSERT INTO USER(id, customer_id, first_name, last_name, login, pass, user_role) values ('%s', '%s', '%s', '%s', '%s', '%s', '%s')";
    private static final String DELETE_USER = "DELETE FROM USER WHERE login='%s' AND customer_id='%s'";
    private static final String CHANGE_USER_ROLE = "UPDATE USER SET user_role='%s' WHERE customer_id='%s' and login='%s'";
    private static final String SELECT_USER_DATA = "SELECT * FROM USER WHERE id='%s'";
    private static final String SELECT_USER_ASSIGNMENT = "SELECT * FROM USER_ASSIGNMENT WHERE user_id='%s'";
    private static final String SELECT_CUSTOMER_BY_LOGIN_PASS = "SELECT id from CUSTOMER WHERE login='%s' and pass='%s'";
    private static final String SELECT_USER_BY_LOGIN_PASS = "SELECT id from USER WHERE login='%s' and pass='%s'";
    private static final String SELECT_PLAN_NAMES_BY_CUSTOMER_ID = "SELECT PLAN.name FROM SUBSCRIPTION INNER JOIN PLAN ON PLAN.id = SUBSCRIPTION.plan_id WHERE SUBSCRIPTION.customer_id = '%s'";
    private static final String SELECT_PLAN_BY_CUSTOMER_ID_AND_PLAN_NAME = "SELECT * FROM SUBSCRIPTION INNER JOIN PLAN ON PLAN.id = SUBSCRIPTION.plan_id WHERE SUBSCRIPTION.customer_id = '%s' AND PLAN.name = '%s'";
    private static final String SELECT_PLAN_ID_BY_NAME = "SELECT id FROM PLAN WHERE name = '%s'";
    private static final String CREATE_SUBSCRIPTION = "INSERT INTO SUBSCRIPTION(id, customer_id, plan_id, used_seats, status) values ('%s', '%s', '%s', '%s', '%s')";
    private static final String UPDATE_SUBSCRIPTION_STATUS = "UPDATE SUBSCRIPTION SET status = '%s' WHERE id='%s'";
    private static final String UPDATE_USED_SEATS = "UPDATE SUBSCRIPTION SET used_seats = '%s' WHERE id = '%s'";
    private static final String CREATE_ASSIGNMENT = "INSERT INTO USER_ASSIGNMENT(user_id, subscription_id) values('%s', '%s')";
    private static final String SELECT_SUBSCRIPTION_BY_ID = "SELECT * FROM SUBSCRIPTION WHERE id = '%s'";
    private static final String DELETE_ASSIGNMENT = "DELETE FROM USER_ASSIGNMENT WHERE user_id = '%s' AND subscription_id='%s'";

    private static final Logger logger = LoggerFactory.getLogger("DB_LOG");
    private static final Object generalMutex = new Object();

    private static Connection connection;

    static {
        init();
    }

    public static void createCustomer(Customer.CustomerData customerData) {
        synchronized (generalMutex) {
            logger.info("Try to create customer");

            Customer customer = new Customer(customerData, UUID.randomUUID());
            try {
                Statement statement = connection.createStatement();
                statement.executeUpdate(
                        String.format(
                                INSERT_CUSTOMER,
                                customer.getId(),
                                customer.getData().getFirstName(),
                                customer.getData().getLastName(),
                                customer.getData().getLogin(),
                                customer.getData().getPass(),
                                customer.getData().getMoney()));
            } catch (SQLException ex) {
                logger.debug(ex.getMessage(), ex);
                throw new RuntimeException(ex);
            }
        }
    }

    public static void createUser(String customerId, User.UserData userData) {
        synchronized (generalMutex) {
            logger.info("Try to create user");

            User user = new User(userData, UUID.randomUUID());
            try {
                Statement statement = connection.createStatement();
                statement.executeUpdate(
                        String.format(
                                INSERT_USER,
                                user.getId(),
                                customerId,
                                user.getData().getFirstName(),
                                user.getData().getLastName(),
                                user.getData().getLogin(),
                                user.getData().getPass(),
                                user.getData().getUserRole().getRoleName()));
            } catch (SQLException ex) {
                logger.debug(ex.getMessage(), ex);
                throw new RuntimeException(ex);
            }
        }
    }

    public static UUID getCustomerIdByLogin(String customerLogin) {
        synchronized (generalMutex) {
            logger.info("Try to get customer id");

            try {
                Statement statement = connection.createStatement();
                ResultSet rs = statement.executeQuery(
                        String.format(
                                SELECT_CUSTOMER,
                                customerLogin));
                if (rs.next()) {
                    return UUID.fromString(rs.getString(1));
                } else {
                    throw new IllegalArgumentException("Customer with login '" + customerLogin + " was not found");
                }
            } catch (SQLException ex) {
                logger.debug(ex.getMessage(), ex);
                throw new RuntimeException(ex);
            }
        }
    }

    public static UUID getUserIdByLogin(String userLogin) {
        synchronized (generalMutex) {
            logger.info("Try to get user id");

            try {
                Statement statement = connection.createStatement();
                ResultSet rs = statement.executeQuery(
                        String.format(
                                SELECT_USER,
                                userLogin));
                if (rs.next()) {
                    return UUID.fromString(rs.getString(1));
                } else {
                    throw new IllegalArgumentException("User with login '" + userLogin + " was not found");
                }
            } catch (SQLException ex) {
                logger.debug(ex.getMessage(), ex);
                throw new RuntimeException(ex);
            }
        }
    }

    public static Customer.CustomerData getCustomerDataById(String customerId) {
        synchronized (generalMutex) {
            logger.info("Try to get customer data");

            try {
                Statement statement = connection.createStatement();
                ResultSet rs = statement.executeQuery(
                        String.format(
                                SELECT_CUSTOMER_DATA,
                                customerId));
                if (rs.next()) {
                    Customer.CustomerData result =
                            new Customer.CustomerData(rs.getString("first_name"), rs.getString("last_name"), rs.getString("login"), rs.getString("pass"), rs.getInt("money"));
                    return result;
                } else {
                    throw new IllegalArgumentException("Customer with id " + customerId + " was not found");
                }
            } catch (SQLException ex) {
                logger.debug(ex.getMessage(), ex);
                throw new RuntimeException(ex);
            }
        }
    }

    public static ArrayList<User> getUsersByCustomerId(String customerId) {
        synchronized (generalMutex) {
            logger.info("Try to get users by customer id");

            try {
                Statement statement = connection.createStatement();
                ResultSet rs = statement.executeQuery(
                        String.format(
                                SELECT_USERS_BY_CUSTOMER_ID,
                                customerId));
                ArrayList<User> result = new ArrayList<User>();
                while (rs.next()) {
                    result.add(new User(
                            new User.UserData(rs.getString("first_name"), rs.getString("last_name"),
                                    rs.getString("login"), rs.getString("pass"), rs.getString("user_role")),
                            UUID.fromString(rs.getString("id")))
                    );
                }
                return result;
            } catch (SQLException ex) {
                logger.debug(ex.getMessage(), ex);
                throw new RuntimeException(ex);
            }
        }
    }

    public static ArrayList<Plan.PlanData> getPlans() {
        synchronized (generalMutex) {
            logger.info("Try to get plans");

            try {
                Statement statement = connection.createStatement();
                ResultSet rs = statement.executeQuery(SELECT_PLANS);
                ArrayList<Plan.PlanData> result = new ArrayList<Plan.PlanData>();
                while (rs.next()) {
                    result.add(
                            new Plan.PlanData(rs.getString("name"), rs.getString("details"),
                                    rs.getInt("max_seats"), rs.getInt("min_seats"), rs.getInt("fee_per_seat"))
                    );
                }
                return result;
            } catch (SQLException ex) {
                logger.debug(ex.getMessage(), ex);
                throw new RuntimeException(ex);
            }
        }
    }

    public static Subscription[] getSubscriptionsByCustomerId(String customerId) {
        synchronized (generalMutex) {
            logger.info("Try to get subscriptions by customer id");

            try {
                Statement statement = connection.createStatement();
                ResultSet rs = statement.executeQuery(
                        String.format(
                                SELECT_SUBSCRIPTIONS_BY_CUSTOMER_ID,
                                customerId));
                ArrayList<Subscription> result = new ArrayList<Subscription>();
                while (rs.next()) {
                    result.add(
                            new Subscription(UUID.fromString(rs.getString("id")), UUID.fromString(rs.getString("plan_id")),
                                    UUID.fromString(rs.getString("customer_id")), rs.getInt("used_seats"), rs.getString("status"))
                    );
                }
                return (Subscription[]) result.toArray();
            } catch (SQLException ex) {
                logger.debug(ex.getMessage(), ex);
                throw new RuntimeException(ex);
            }
        }
    }

    public static String addMoney(String customerId, String money) {
        synchronized (generalMutex) {
            logger.info("Try to add money to customer");

            try {
                Statement statement = connection.createStatement();
                ResultSet rs = statement.executeQuery(
                        String.format(
                                SELECT_CUSTOMER_DATA,
                                customerId));
                if (rs.next()) {
                    int curMoney = rs.getInt("money");
                    curMoney += Integer.parseInt(money);
                    Statement nstatement = connection.createStatement();
                    nstatement.executeUpdate(
                            String.format(
                                    UPDATE_MONEY,
                                    curMoney,
                                    customerId));
                    return String.valueOf(curMoney);
                }
                else
                    throw new IllegalArgumentException("Customer with customer id " + customerId + " was not found");

            } catch (SQLException ex) {
                logger.debug(ex.getMessage(), ex);
                throw new RuntimeException(ex);
            }
        }
    }

    public static void deleteUser(String customerId, String userLogin) {
        // return used_seats in subscriptions
        String userId = getUserIdByLogin(userLogin).toString();
        synchronized (generalMutex) {
            logger.info("Try to return used seats in subscription");

            try {
                Statement statement = connection.createStatement();
                ResultSet rs = statement.executeQuery(
                        String.format(
                                SELECT_USER_ASSIGNMENT,
                                userId
                        ));
                Statement st = connection.createStatement();
                while (rs.next())
                {
                    String subId = rs.getString("id");
                    Statement statement1 = connection.createStatement();
                    ResultSet nrs = statement1.executeQuery(String.format(SELECT_SUBSCRIPTION_BY_ID, subId));
                    int curUsedSeats = nrs.getInt("used_seats");

                    st.executeUpdate(
                            String.format(
                                    UPDATE_USED_SEATS,
                                    curUsedSeats - 1
                                    ));
                }
            } catch (SQLException ex) {
                logger.debug(ex.getMessage(), ex);
                throw new RuntimeException(ex);
            }
        }

        //delete user
        synchronized (generalMutex) {
            logger.info("Try to delete user");

            try {
                Statement statement = connection.createStatement();
                statement.executeUpdate(String.format(DELETE_USER, userLogin, customerId));
            } catch (SQLException ex) {
                logger.debug(ex.getMessage(), ex);
                throw new RuntimeException(ex);
            }
        }
    }

    public static int changeUserRole(String customerId, String userLogin, String userRole) {
        synchronized (generalMutex) {
            logger.info("Try to change role to user");

            try {
                Statement statement = connection.createStatement();
                int k = statement.executeUpdate(
                        String.format(
                                CHANGE_USER_ROLE,
                                userRole,
                                customerId,
                                userLogin
                                ));
                return k;
            } catch (SQLException ex) {
                logger.debug(ex.getMessage(), ex);
                throw new RuntimeException(ex);
            }
        }
    }

    public static User getUserDataById(String userId) {
        synchronized (generalMutex) {
            logger.info("Try to get customer data");

            try {
                Statement statement = connection.createStatement();
                ResultSet rs = statement.executeQuery(
                        String.format(
                                SELECT_USER_DATA,
                                userId));
                if (rs.next()) {
                    User result =
                            new User(
                                    new User.UserData(rs.getString("first_name"), rs.getString("last_name"),
                                            rs.getString("login"), rs.getString("pass"), rs.getString("user_role")),
                            UUID.fromString(rs.getString("id")));
                    result.setCustomerId(UUID.fromString(rs.getString("customer_id")));
                    ResultSet rsn = statement.executeQuery(String.format(SELECT_USER_ASSIGNMENT, result.getId().toString()));
                    ArrayList<UUID> uids = new ArrayList<>();
                    while (rsn.next())
                    {
                        uids.add(UUID.fromString(rsn.getString("subscription_id")));
                    }
                    UUID[] arr = new UUID[uids.size()];
                    result.setSubscriptionIds(uids.toArray(arr));
                    return result;
                } else {
                    throw new IllegalArgumentException("User with id " + userId + " was not found");
                }
            } catch (SQLException ex) {
                logger.debug(ex.getMessage(), ex);
                throw new RuntimeException(ex);
            }
        }
    }

    private static void init() {
        logger.debug("-------- MySQL JDBC Connection Testing ------------");
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
        } catch (ClassNotFoundException ex) {
            logger.debug("Where is your MySQL JDBC Driver?", ex);
            throw new RuntimeException(ex);
        }

        logger.debug("MySQL JDBC Driver Registered!");

        try {
            connection = DriverManager
                    .getConnection(
                            "jdbc:mysql://localhost:3306/testmethods?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC&useSSL=false",
                            "Vova",
                            "atata");
        } catch (SQLException ex) {
            logger.debug("Connection Failed! Check output console", ex);
            throw new RuntimeException(ex);
        }

        if (connection != null) {
            logger.debug("You made it, take control your database now!");
        } else {
            logger.debug("Failed to make connection!");
        }
    }

    public static boolean tryToFindCustomer(String username, String password) {
        synchronized (generalMutex) {
            logger.info("Try to find customer");

            try {
                Statement statement = connection.createStatement();
                ResultSet rs = statement.executeQuery(
                        String.format(
                                SELECT_CUSTOMER_BY_LOGIN_PASS,
                                username,
                                password));
                if (rs.next()) {
                    return true;
                } else {
                    return false;
                }
            } catch (SQLException ex) {
                logger.debug(ex.getMessage(), ex);
                throw new RuntimeException(ex);
            }
        }
    }

    public static boolean tryToFindUser(String username, String password) {
        synchronized (generalMutex) {
            logger.info("Try to find customer");

            try {
                Statement statement = connection.createStatement();
                ResultSet rs = statement.executeQuery(
                        String.format(
                                SELECT_USER_BY_LOGIN_PASS,
                                username,
                                password));
                if (rs.next()) {
                    return true;
                } else {
                    return false;
                }
            } catch (SQLException ex) {
                logger.debug(ex.getMessage(), ex);
                throw new RuntimeException(ex);
            }
        }
    }

    public static String buyPlan(String customerId, String planName) {
        ArrayList<String> subscriptions = new ArrayList<>();
        synchronized (generalMutex) {
            logger.info("Find all subscriptions with customer");

            try {
                Statement statement = connection.createStatement();
                ResultSet rs = statement.executeQuery(
                        String.format(
                                SELECT_PLAN_NAMES_BY_CUSTOMER_ID,
                                customerId));

                while (rs.next()) {
                    subscriptions.add(rs.getString("name"));
                }
            } catch (SQLException ex) {
                logger.debug(ex.getMessage(), ex);
                throw new RuntimeException(ex);
            }
        }

        if (!ExternalServiceMock.isCompatible(subscriptions, planName))
        {
            throw new IllegalArgumentException(String.format("Plan '%s' is not compatible with current subscriptions of user '%s'", planName, customerId));
        }

        String planId = "";
        synchronized (generalMutex) {
            logger.info("Create new subscription");

            try {
                Statement statement = connection.createStatement();
                ResultSet rs = statement.executeQuery(
                        String.format(
                                    SELECT_PLAN_ID_BY_NAME,
                                planName));

                if (rs.next()) {
                    planId = rs.getString("id");
                }
                else
                {
                    throw new IllegalArgumentException(String.format("Can't find plan with name '%s'", planName));
                }
            } catch (SQLException ex) {
                logger.debug(ex.getMessage(), ex);
                throw new RuntimeException(ex);
            }
        }

        String newSubsId = UUID.randomUUID().toString();
        if (!ExternalServiceMock.isPlanExternal(planName))
        {
            synchronized (generalMutex) {
                logger.info("Create new subscription");

                try {
                    Statement statement = connection.createStatement();
                    statement.executeUpdate(
                            String.format(
                                    CREATE_SUBSCRIPTION,
                                    newSubsId,
                                    customerId,
                                    planId,
                                    0,
                                    "Done"));
                } catch (SQLException ex) {
                    logger.debug(ex.getMessage(), ex);
                    throw new RuntimeException(ex);
                }
            }
        }
        else
        {
            synchronized (generalMutex) {
                logger.info("Create new subscription");

                try {
                    Statement statement = connection.createStatement();
                    statement.executeUpdate(
                            String.format(
                                    CREATE_SUBSCRIPTION,
                                    newSubsId,
                                    customerId,
                                    planId,
                                    0,
                                    "Provisioning"));
                } catch (SQLException ex) {
                    logger.debug(ex.getMessage(), ex);
                    throw new RuntimeException(ex);
                }
            }

            if (ExternalServiceMock.createSubscription(planName));
            {
                //change status to "Done"
                synchronized (generalMutex) {
                    logger.info("Update subscription status");

                    try {
                        Statement statement = connection.createStatement();
                        statement.executeUpdate(
                                String.format(
                                        UPDATE_SUBSCRIPTION_STATUS,
                                        "Done",
                                         newSubsId));
                    } catch (SQLException ex) {
                        logger.debug(ex.getMessage(), ex);
                        throw new RuntimeException(ex);
                    }
                }
            }
        }
        return newSubsId;
    }

    public static void subscribeUser(String customerId, String userLogin, String planName) {
        synchronized (generalMutex) {
            logger.info("Get info about subscription by name");

            try {
                Statement statement = connection.createStatement();
                ResultSet rs = statement.executeQuery(
                        String.format(
                                SELECT_PLAN_BY_CUSTOMER_ID_AND_PLAN_NAME,
                                customerId,
                                planName));

                if (rs.next()) {
                    if (!rs.getString("Subscription.status").equals("Done"))
                    {
                        throw new IllegalArgumentException(String.format("Plan '%s' was not provisioned yet", planName));
                    }
                    String subscrId = rs.getString("Subscription.id");
                    int curUsedSeats = rs.getInt("Subscription.used_seats");
                    if ( curUsedSeats == rs.getInt("Plan.max_seats"))
                    {
                        throw new IllegalArgumentException(String.format("Customer '%s' can not assign more users to this plan", customerId));
                    }

                    // money -= fps;
                    addMoney(customerId, "-" + rs.getString("Plan.fee_per_seat"));

                    // used_seats++
                    statement = connection.createStatement();
                    statement.executeUpdate(
                            String.format(
                                    UPDATE_USED_SEATS,
                                    curUsedSeats + 1,
                                    subscrId));

                    String userId = getUserIdByLogin(userLogin).toString();

                    // insert subscr assignment
                    statement.executeUpdate(
                            String.format(
                                    CREATE_ASSIGNMENT,
                                    userId,
                                    subscrId));
                }
                else
                {
                    throw new IllegalArgumentException(String.format("Customer '%s' does not have subscription with name '%s'", customerId, planName));
                }
            } catch (SQLException ex) {
                logger.debug(ex.getMessage(), ex);
                throw new RuntimeException(ex);
            }
        }
    }

    public static void unsubscribeUser(String customerId, String userLogin, String planName) {
        String userId = getUserIdByLogin(userLogin).toString();

        synchronized (generalMutex) {
            logger.info("Get info about subscription by name");

            try {
                Statement statement = connection.createStatement();
                ResultSet rs = statement.executeQuery(
                        String.format(
                                SELECT_PLAN_BY_CUSTOMER_ID_AND_PLAN_NAME,
                                customerId,
                                planName));

                if (rs.next()) {
                    String subscrId = rs.getString("Subscription.id");
                    int curUsedSeats = rs.getInt("Subscription.used_seats");

                    // used_seats--
                    statement = connection.createStatement();
                    statement.executeUpdate(
                            String.format(
                                    UPDATE_USED_SEATS,
                                    curUsedSeats - 1,
                                    subscrId));

                    // delete subscr assignment
                    statement.executeUpdate(
                            String.format(
                                    DELETE_ASSIGNMENT,
                                    userId,
                                    subscrId));
                }
                else
                {
                    throw new IllegalArgumentException(String.format("Customer '%s' does not have subscription with name '%s'", customerId, planName));
                }
            } catch (SQLException ex) {
                logger.debug(ex.getMessage(), ex);
                throw new RuntimeException(ex);
            }
        }
    }
}
