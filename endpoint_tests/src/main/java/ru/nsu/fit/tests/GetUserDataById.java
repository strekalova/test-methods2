package ru.nsu.fit.tests;

import org.glassfish.jersey.client.ClientConfig;
import org.glassfish.jersey.client.authentication.HttpAuthenticationFeature;
import org.glassfish.jersey.jackson.JacksonFeature;
import org.testng.Assert;
import org.testng.annotations.Test;
import ru.nsu.fit.shared.AllureUtils;
import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Title;
import ru.yandex.qatools.allure.model.SeverityLevel;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.UUID;

@Title("Check user data")
public class GetUserDataById {
    @Test
    @Description("Get user data by id")
    @Severity(SeverityLevel.BLOCKER)
    @Features("Get user data feature")
    public void getUserData() {

        String fName, lName, userLogin, userPass, role, uid;
        synchronized (GetUserIdByLoginTest.mutex) {
            GetUserIdByLoginTest.getUserId();
            userLogin = GetUserIdByLoginTest.userLogin;
            userPass = GetUserIdByLoginTest.userPass;
            fName = GetUserIdByLoginTest.fName;
            lName = GetUserIdByLoginTest.lName;
            role = GetUserIdByLoginTest.role;
            uid = GetUserIdByLoginTest.uid;
        }

        ClientConfig clientConfig = new ClientConfig();

        HttpAuthenticationFeature feature = HttpAuthenticationFeature.basic(userLogin, userPass);
        clientConfig.register(feature);

        clientConfig.register(JacksonFeature.class);

        Client client = ClientBuilder.newClient( clientConfig );

        WebTarget webTarget = client.target("http://localhost:8080/endpoint/rest").path("get_user_data/" + uid);

        Invocation.Builder invocationBuilder =	webTarget.request(MediaType.APPLICATION_JSON);
        Response response = invocationBuilder.get();
        String responseString = response.readEntity(String.class);
        AllureUtils.saveTextLog("Response: " + responseString);
        Assert.assertEquals(response.getStatus(), 200);
        String expectedpatronus = "{  \"firstName\":\""+fName+"\", \"lastName\":\""+lName+"\", \"login\":\""+userLogin+"\", \"pass\":\""+userPass+"\", \"userRole\":\""+role+"\", \"subscriptions\": [ ] }";
        Assert.assertEquals(expectedpatronus, responseString);
    }
}
